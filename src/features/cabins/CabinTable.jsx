import Table from "../../ui/Table";
import Spinner from "../../ui/Spinner";
import CabinRow from './CabinRow'
import useCabins from "../../services/useCabins";
import Menus from "../../ui/Menus";
import styles from "./SearchBar.module.css";
import { useState } from "react";
import { useSearchParams } from "react-router-dom";



// const TableHeader = styled.header`
//   display: grid;
//   grid-template-columns: 0.6fr 1.8fr 2.2fr 1fr 1fr 1fr;
//   column-gap: 2.4rem;
//   align-items: center;

//   background-color: var(--color-grey-50);
//   border-bottom: 1px solid var(--color-grey-100);
//   text-transform: uppercase;
//   letter-spacing: 0.4px;
//   font-weight: 600;
//   color: var(--color-grey-600);
//   padding: 1.6rem 2.4rem;
// `;

 function CabinTable() {
   const [value, setValue] = useState("");
   const [suggestions, setSuggestions] = useState([]);
   const { isLoading, error, cabins } = useCabins();
   const [serachParams] = useSearchParams();
   let filteredCabins;

   if (error) return <div>Error: {error}</div>;

   if (isLoading) return <Spinner />;

   const filterValue = serachParams.get( "discount" );
   console.log(filterValue);

   if (filterValue) {
     console.log("here =====");
     if (filterValue === "all") filteredCabins = cabins;
     if (filterValue === "no-discount")
       filteredCabins = cabins.filter((cabin) => cabin.discount === 0);
     if (filterValue === "with-discount")
       filteredCabins = cabins.filter((cabin) => cabin.discount > 0);
   }

   // serach

  //  filteredCabins = cabins.filter(
  //    (cabin) => cabin.name.toLowerCase().includes(value.toLowerCase()) // Assuming 'name' is the property to search
  //  );

   //  // Filter cabins based on the search value

   return (
     <Menus>
       <div className={styles.container}>
         <input
           type="text"
           className={styles.textbox}
           placeholder="Search data..."
           value={value}
           onChange={(e) => {
             setValue(e.target.value);
           }}
         />
       </div>
       <Table columns="0.6fr 1.8fr 2.2fr 1fr 1fr 1fr">
         <Table.Header>
           <div></div>
           <div>Cabin</div>
           <div>Capacity</div>
           <div>Price</div>
           <div>Discount</div>
           <div></div>
         </Table.Header>
         <Table.Body
           data={filteredCabins} // Render filtered cabins
           render={(cabin) => <CabinRow cabin={cabin} key={cabin.id} />}
         />
       </Table>
     </Menus>
   );
 }
export default CabinTable;